package InnerClass;

/**
 * Created by Bryan on 24/07/2017.
 */
abstract class Persona{
    abstract void comer();
}
class Anonymus{
    public static void main(String args[]){
        Persona p=new Persona(){
            void comer(){System.out.println("futas dulces");}
        };
        p.comer();
    }
}
/*
Ejmeplo de una clase anonima
Una clase que no tiene nombre es conocida como clase interna anónima en java.
Se debe utilizar si tiene que anular el método de clase o interfaz. Java La clase interna anónima se puede crear de dos maneras:
Clase (puede ser abstracta o concreta).
Interfaz
* */