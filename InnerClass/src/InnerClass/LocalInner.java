package InnerClass;

/**
 * Created by Bryan on 24/07/2017.
 */
public class LocalInner {

    private int data=30;//instance variable
    void display(){
        class Local{
            void msg(){System.out.println(data);}
        }
        Local l=new Local();
        l.msg();
    }
    public static void main(String args[]){
        LocalInner obj=new LocalInner();
        obj.display();
    }
}

/*
Ejmeplo de creacion de inner local class
Una clase, es decir, creada dentro de un método, se llama clase interna local en java.
Si desea invocar los métodos de la clase interna local, debe instanciar esta clase dentro del método.
* */