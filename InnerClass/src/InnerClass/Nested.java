package InnerClass;

/**
 * Created by Bryan on 24/07/2017.
 */
class Nested{
    static int valor=30;
    static class Inner{
        void msg(){System.out.println("El sato es: "+ valor);}
    }
    public static void main(String args[]){
        Nested.Inner obj=new Nested.Inner();
        obj.msg();
    }
}
/*
En este ejemplo dentro de una clase anidada
Una clase estática, es decir, creada dentro de una clase, se llama clase anidada estática en java.
No puede tener acceso a miembros y métodos de datos no estáticos.
Se puede acceder por el nombre de clase exterior.
Puede acceder a los miembros de datos estáticos de la clase externa incluyendo privado.
La clase anidada estática no puede tener acceso a miembro o método de datos no estáticos (instancia).
* */