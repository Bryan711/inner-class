package InnerClass;

/**
 * Created by Bryan on 24/07/2017.
 */
public class InnerClass {

    private final static int SIZE = 15;
    private int[] arrayOfInts = new int[SIZE];

    public InnerClass() {
        for (int i = 0; i < SIZE; i++) {
            arrayOfInts[i] = i;
        }
    }

    public void printEven() {
        InnerEvenIterator iterator = this.new InnerEvenIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.getNext() + " ");
        }
    }

    private class InnerEvenIterator {
        private int next = 0;
        public boolean hasNext() {
            return (next <= SIZE - 1);
        }

        public int getNext() {
            int retValue = arrayOfInts[next];
            next += 2;
            return retValue;
        }
    }

    public static void main(String s[]) {
        InnerClass ds = new InnerClass();
        ds.printEven();

    }
}
/*
Ejemplo de creacion de una outer class e inner class
el método printEven, se apoya en una clase interna, llamada InnerEvenIterator,
que básicamente accede a los datos de DataStructure y mediante una variable
iterador interna los va recorriendo de dos en dos e imprimiéndolos.
* */
